/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package progam1;

class Test {
    
    public Test(int val) {
        value = val;
    }
    
    @Override
    public String toString() {
        return " current int value " + value;
    }
   
    private int value;
}

/**
 *
 * @author bieron_1034494
 */
public class Progam1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Test testClass = new Test(10);
        
        System.out.println(testClass);
    }
    
}
